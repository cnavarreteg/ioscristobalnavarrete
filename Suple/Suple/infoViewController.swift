//
//  DataViewController.swift
//  Suple
//
//  Created by Cristobal Navarrete on 21/8/18.
//  Copyright © 2018 epn. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var addressLine2: UILabel!
    @IBOutlet weak var city: UILabel!
    
    var itemInfoPerson:(itemManagerPerson: PersonManager, index: Int)?
    
    override func viewDidLoad() {
       addressLine1.text = itemInfoPerson?.itemManagerPerson.itemsPerson[(itemInfoPerson?.index)!].address.addressLine1
        addressLine2.text = itemInfoPerson?.itemManagerPerson.itemsPerson[(itemInfoPerson?.index)!].address.addressLine2
        city.text = itemInfoPerson?.itemManagerPerson.itemsPerson[(itemInfoPerson?.index)!].address.city
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func deletePressed(_ sender: Any) {
        itemInfoPerson?.itemManagerPerson.deletePerson(index: (itemInfoPerson?.index)!)
        navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
