//
//  Json.swift
//  Suple
//
//  Created by Cristobal Navarrete on 21/8/18.
//  Copyright © 2018 epn. All rights reserved.
//

import Foundation

struct JsonInfo: Decodable {
    let data:[Person]
}

struct Person: Decodable {
    let firstName: String
    let lastName: String
    let email: String
    let address: Address
}

struct Address: Decodable {
    var addressLine1: String
    var addressLine2: String
    var city: String
}
