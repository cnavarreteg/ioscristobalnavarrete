//
//  ViewController.swift
//  Supletorio
//
//  Created by Cristobal Navarrete on 21/8/18.
//  Copyright © 2018 epn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var personTableView: UITableView!
    let personManager = PersonManager()
    
    override func viewDidLoad() {
        let network: Network = Network()
        network.getAllPerson() {
            (personArray) in
            self.personManager.itemsPerson = personArray
            self.personTableView.reloadData()
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        personTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personManager.itemsPerson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = personTableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! PersonTableViewCell
        cell.fillData(label: personManager.itemsPerson[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDataSegue" {
            let destination = segue.destination as! DataViewController
            let selectedRow = personTableView.indexPathsForSelectedRows![0]
            destination.itemInfoPerson = (personManager, selectedRow.row)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toDataSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

