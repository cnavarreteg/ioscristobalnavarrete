//
//  Network.swift
//  Suple
//
//  Created by Cristobal Navarrete on 21/8/18.
//  Copyright © 2018 epn. All rights reserved.
//

import Foundation
import Alamofire

class Network {
    func getAllPerson(completion:@escaping ([Person])->()) {
        var personArray:[Person] = []
        Alamofire.request("https://private-534ec-amstronghuang.apiary-mock.com/members").responseJSON {
            response in
            guard let data = response.data else {
                print("ERROR")
                return
            }
            
            guard let person = try? JSONDecoder().decode(JsonInfo.self, from: data) else {
                print("error decoding Person")
                return
            }
            
            for persona in person.data {
                let personAux = Person(firstName: persona.firstName, lastName: persona.lastName, email: persona.email, address: persona.address)
            
                
                     personArray.append(personAux)
                
                
                
            }
         
            completion(personArray)
        }
    }
}
